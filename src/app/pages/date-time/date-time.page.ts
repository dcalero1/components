import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-date-time',
  templateUrl: './date-time.page.html',
  styleUrls: ['./date-time.page.scss'],
})
export class DateTimePage implements OnInit {
  birthDate: Date = new Date();
  customYearValues = [2025, 2020, 2016, 2008, 2004, 2000, 1996];
  customPicketOptions = {
    buttons: [{
                text: 'Hola',
                handler: (event) => {
                  console.log(event);
                }
              },
              {
                text: 'Mundo',
                handler: (event) => {
                  console.log('log!');
                }
              }
              ]
  };
  constructor() { }

  ngOnInit() {
  }
  changeDate(newDate) {
    console.log({newDate: newDate});
    console.log(new Date(newDate.detail.value));
  }
}
